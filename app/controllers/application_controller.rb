class ApplicationController < ActionController::Base
  include SessionsHelper
  
  protect_from_forgery
  helper_method :current_user, :user_signed_in?

  def privacy
    render 'static/privacy'
  end

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
    rescue ActiveRecord::RecordNotFound
      session.delete(:user_id)
      nil
  end

  def user_signed_in?
    !current_user.nil?
  end

  def authenticate!
    user_signed_in? || redirect_to(login_url, notice: "Você precisa estar autenticado...")
  end  

  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Por favor, efetue o login."
      redirect_to login_url
    end
  end
  
end
