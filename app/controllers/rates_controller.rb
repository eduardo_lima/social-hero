class RatesController < ApplicationController
  before_action :logged_in_user, only: [:create]

  before_action :load_gamekeeper_settings, only: [:create]

  def create
    @rate = Rate.new
    @rate.user_id = current_user.id
    @rate.rate_number = params[:rate][:rate_number]
    @rate.rate_description = params[:rate][:rate_description]

    options = {}
    options[:player_id] = current_user.uid;
    options[:performed_action] = {}
    options[:performed_action][:verb] = "avaliar"
    options[:performed_action][:object_properties] = {}
    options[:performed_action][:object_properties] = []
    options[:performed_action][:object_properties] << {
      name: "nota",
      value: @rate.rate_number
    }
    options[:performed_action][:object_properties] << {
      name: "descricao",
      value: @rate.rate_description
    } 

    begin
      @gk.perform_action(current_user, options)
    rescue => exception

      flash[:danger] = "Falha ao enviar os ação para o Gamekeeper"

      redirect_to root_url
      return false # testar
    end
    
    if @rate.save then
      current_user.points_obtained += 200

      flash[:success] = "Muito obrigado, em troca disso você ganhou mais 200 pontos..."
      redirect_to root_url
    else
      flash[:success] = "Não conseguimos obter tua avaliação..."
      redirect_to root_url
    end
  end

private

  def load_gamekeeper_settings
    token = GAMEKEEPER_CONFIG['ACCESS_TOKEN']
    name = GAMEKEEPER_CONFIG['GAME_UNIQUE_NAME']

    @gk = GamekeeperApiConsumer.new(name, token)
  end
end
