class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  before_action :load_gamekeeper_settings, only: [:create]

  def create
    # colocar aqui post feed do face

    @micropost = current_user.microposts.build(micropost_params)

    options = {}
    options[:player_id] = current_user.uid;
    options[:performed_action] = {}
    options[:performed_action][:verb] = "publicar"
    options[:performed_action][:object_properties] = {}
    options[:performed_action][:object_properties] = []
    options[:performed_action][:object_properties] << {
      name: "autor",
      value: current_user.name
    }
    options[:performed_action][:object_properties] << {
      name: "conteudo",
      value: @micropost.content
    } 

    begin
      @gk.perform_action(current_user, options)
    rescue => exception

      flash[:danger] = "Falha durante a publicação..."

      @feed_items = current_user.feed.paginate(page: params[:page])
      render 'users/home'
      return false # testar
    end

    if @micropost.save then

      current_user.points_obtained += 100
      current_user.save

      flash[:success] = "Parabéns, você ganhou 100 pontos! Veja seu perfil..."
      redirect_to root_url
    else
      # não tá legal assim, mas funciona
      @feed_items = current_user.feed.paginate(page: params[:page])
      render 'users/home'
      # render partial: 'users/home'
      # redirect_to root_url
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Post removido"
    redirect_to root_url
  end

private

  def micropost_params
    params.require(:micropost).permit(:content, :picture)
  end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end

  def load_gamekeeper_settings
    token = GAMEKEEPER_CONFIG['ACCESS_TOKEN']
    name = GAMEKEEPER_CONFIG['GAME_UNIQUE_NAME']

    @gk = GamekeeperApiConsumer.new(name, token)
  end
end
