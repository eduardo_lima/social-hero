class UsersController < ApplicationController
  include SessionsHelper
  before_action :logged_in_user, only: [:index, :home, :show, :edit, :update,
                                        :following, :followers]
  before_action :load_gamekeeper_settings, only: [:index, :show, :following, :followers]
  before_action :fill_player_stats, only: [:show]
  # before_action :correct_user,   only: [:edit, :update] 

##----------------------------------------------------------------------------------------------------------------
  def new
  end

##----------------------------------------------------------------------------------------------------------------
  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

##----------------------------------------------------------------------------------------------------------------
  def index
    @users = User.paginate(page: params[:page], per_page: 10).order('points_obtained DESC')
  rescue ActiveRecord::RecordNotFound  
    redirect_to root_url
    return
  end

##----------------------------------------------------------------------------------------------------------------
  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])

    hash = @gk.player_stats(@user)['data']

    puts hash.inspect

    unless hash.blank? or hash.empty? then
      @player_stats.level_name = hash['level_name']
      @player_stats.level_photo = hash['level_photo']
      @player_stats.points = hash['points_obtained']
      @player_stats.badges = hash['badges_obtained']['data'] ? hash['badges_obtained']['data'] : [] 
    end

  rescue RestClient::Unauthorized
    flash[:danger] = "Falha ao pegar os pontos do Gamekeeper: Token não autorizado."
    return
  rescue ActiveRecord::RecordNotFound  
    redirect_to root_url
    return
  rescue => e
    puts e.inspect

    redirect_to root_url
    return
  end

##----------------------------------------------------------------------------------------------------------------
  def following
    @title = "Seguindo"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page]).order('points_obtained DESC')
    
    render 'show_follow'

  rescue RestClient::Unauthorized
    flash[:danger] = "Falha ao pegar os pontos do Gamekeeper: Token não autorizado."
    return
  rescue ActiveRecord::RecordNotFound  
    redirect_to root_url
    return
  end

##----------------------------------------------------------------------------------------------------------------
  def followers
    @title = "Seguidores"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page]).order('points_obtained DESC')

    render 'show_follow'

  rescue RestClient::Unauthorized
    flash[:danger] = "Falha ao pegar os pontos do Gamekeeper: Token não autorizado."
    return
  rescue ActiveRecord::RecordNotFound  
    redirect_to root_url
    return
  end

##----------------------------------------------------------------------------------------------------------------
  def rate
    unless current_user.rate 
      @rate = Rate.new
    else
      flash[:warning] = "Você já availou o Social Hero."
      redirect_to root_url
    end
  end


private 

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?
  end
##----------------------------------------------------------------------------------------------------------------
  def load_gamekeeper_settings
    token = GAMEKEEPER_CONFIG['ACCESS_TOKEN']
    name = GAMEKEEPER_CONFIG['GAME_UNIQUE_NAME']

    @gk = GamekeeperApiConsumer.new(name, token)
  end
##----------------------------------------------------------------------------------------------------------------
  def fill_player_stats
    @player_stats = PlayerStats.new
    @player_stats.level_name = "----------------"
    @player_stats.points = "----------------"
  end
  

end
