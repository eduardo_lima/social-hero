class SessionsController < ApplicationController
  include SessionsHelper

  before_action :load_gamekeeper_settings, only: [:create]
  
  def create
    auth = request.env["omniauth.auth"]
    # user = User.find_or_create_with_omniauth(auth)

    user = User.find_by_provider_and_uid(auth[:provider], auth[:uid]) || User.new

    # se ainda não inseriu o usuário na base, manda pro gamekeeper
    if user.new_record? then
      user.name = auth[:info][:name]
      user.email = auth[:info][:email]
      user.access_token = auth[:credentials][:token]
      user.uid = auth[:uid]
      user.photo_url = auth[:info][:image]
      user.provider = auth[:provider]
      user.points_obtained = 20

      @gk.add_player(user.name, user.uid, user.photo_url)
      
      user.save
    end

    session[:user_id] = user.id
    flash[:success] = "Sinta-se a vontade no Social Hero ;)"
    redirect_back_or root_url
  rescue => exception
    puts exception.inspect

    flash[:danger] = "Erro na comunicação com o Gamekeeper"

    redirect_to login_url
  end

  def failure
    redirect_to login_url
  end

  def destroy
    log_out if logged_in?

    flash[:success] = "Até logo ;)"
    redirect_to login_url
  end

  private

  def load_gamekeeper_settings
    token = GAMEKEEPER_CONFIG['ACCESS_TOKEN']
    name = GAMEKEEPER_CONFIG['GAME_UNIQUE_NAME']

    @gk = GamekeeperApiConsumer.new(name, token)
  end
end
