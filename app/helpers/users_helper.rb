module UsersHelper

  def photo_for(user, options = { size: "60x60" }) 
    image_tag(user.photo_url, size: options[:size], title: user.name, class: "photo_url")
  end
end
