# encoding: utf-8

class PictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  process resize_to_limit: [400, 400]

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  # storage :file
  storage :dropbox
  # storage :fog

#   if Rails.env.production?
#   storage :fog
# else
#   storage :file
# end

# if Rails.env.production?
  # CarrierWave.configure do |config|
  #   config.fog_credentials = {
  #     # Configuration for Amazon S3
  #     :provider              => 'AWS',
  #     :aws_access_key_id     => ENV['S3_ACCESS_KEY'],
  #     :aws_secret_access_key => ENV['S3_SECRET_KEY']
  #   }
  #   config.fog_directory     =  ENV['S3_BUCKET']
  # end
# end

CarrierWave.configure do |config|
  config.dropbox_app_key = DROPBOX_CONFIG["APP_KEY"]
  config.dropbox_app_secret = DROPBOX_CONFIG["APP_SECRET"]
  config.dropbox_access_token = DROPBOX_CONFIG["ACCESS_TOKEN"]
  config.dropbox_access_token_secret = DROPBOX_CONFIG["ACCESS_TOKEN_SECRET"]
  config.dropbox_user_id = DROPBOX_CONFIG["USER_ID"]
  config.dropbox_access_type = "app_folder"
end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
