class GamekeeperApiConsumer

  def initialize(game, token)
    @token = token
    @game = game
  end

  # rota POST https://furb-gamekeeper.herokuapp.com/api/v1/:game/players
  def add_player(name, uid, photo_url = nil)
    # monta a url
    url = build_url + "players"

    params = {}
    params[:access_token] = @token
    params[:name] = name
    params[:social_id] = uid
    params[:image_url] = photo_url if photo_url.present?

    puts "Adicionando player na url"
    puts url
    puts params.inspect

    puts JSON.parse RestClient.post(url, params)
  end

  # rota POST https://furb-gamekeeper.herokuapp.com/api/v1/:game/players/:player_id/perform
  def perform_action(user, options)
    url = build_url + "players/#{user.uid}/perform"

    params = {}
    params[:access_token] = @token
    params.merge!(options)

    puts "Adicionando execução de ação na url"
    puts url

    puts JSON.parse RestClient.post(url, params)
  end

  def player_stats(user)
    url = build_url + "players/#{user.uid}.json?access_token=#{@token}"

    puts "Obtendo player stats na url"
    puts url

    return JSON.parse RestClient.get(url)
  end

  def overall_rankings
    url = build_url + "leaderboards/overall_rankings.json?access_token=#{@token}"

    puts "Obtendo rankings na url"
    puts url

    return JSON.parse RestClient.get(url)
  end

  private

  def build_url
    "https://furb-gamekeeper.herokuapp.com/api/v1/#{@game}/"
  end

end