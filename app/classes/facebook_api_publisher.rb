class FacebookPublisher

   API_VERSION = 'v2.8'
   BASE_API_URL = 'https://graph.facebook.com/'

  def self.post(access_token, message, link = nil)

    url = "#{BASE_API_URL}#{API_VERSION}/me/feed/"

    content = {}

    content[:access_token] = access_token
    content[:message] = message
    content[:link] = link if link

    JSON.parse(RestClient.post(url, content))
    
    # response = JSON.parse(RestClient.post(url, content))      
    # return PublicationResponse.new(true, nil, response["id"], parent_object_id)

    return true
  rescue => exception
    return false
  end

end