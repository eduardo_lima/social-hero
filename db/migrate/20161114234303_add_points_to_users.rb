class AddPointsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :points_obtained, :integer, default: 0
  end
end
