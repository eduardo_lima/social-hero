class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.string :rate_number
      t.string :rate_description
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
