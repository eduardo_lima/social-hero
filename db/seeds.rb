99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               uid: [0..100000].sample,
               access_token: SecureRandom.hex(10))
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

  # validates :email, :access_token, :name, :uid, presence: true
  # validates :email, uniqueness: true