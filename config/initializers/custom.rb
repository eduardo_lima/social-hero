FACE_CONFIG = YAML.load_file(Rails.root.join('config/facebook-config.yml'))
GAMEKEEPER_CONFIG = YAML.load_file(Rails.root.join('config/gamekeeper-config.yml'))
DROPBOX_CONFIG = YAML.load_file(Rails.root.join('config/dropbox-config.yml'))