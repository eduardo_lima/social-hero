Rails.application.config.middleware.use OmniAuth::Builder do
  if Rails.env.development?
    provider :facebook, FACE_CONFIG["APP_KEY_ENV"], FACE_CONFIG["APP_SECRET_ENV"], 
      :scope => "email",
      :image_size => "normal",
      :secure_image_url => true
  else 
    provider :facebook, FACE_CONFIG["APP_KEY"], FACE_CONFIG["APP_SECRET"],
      :scope => "email",
      :image_size => "normal",
      :secure_image_url => true
  end
end