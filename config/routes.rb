Rails.application.routes.draw do

  get 'rates_controller/create'

  root to: "users#home"

  get "/privacy" => "application#privacy"

  get "login" => "users#new", as: :login

  get "/auth/:provider/callback" => "sessions#create", as: :auth_callback
  get "/auth/failure" => "sessions#failure", as: :auth_failure
  delete "/logout" => "sessions#destroy", as: :logout

  resources :microposts#, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]

  resources :rates

  #nao acabou o tutorial

  resources :users do
    member do
      get :following, :followers
      get :rate
    end
  end

end
